import React, { createContext, useContext, useEffect, useState } from 'react'

import Cookies from 'cookies'
import axios from 'axios'

const authContext = createContext()

export function AuthProvider({ children }) {
  const auth = useProvideAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

export const useAuth = () => {
  return useContext(authContext)
}

export const checkUserServerSide = async (req, res) => {
  const cookies = new Cookies(req, res)

  const jwtToken = cookies.get('strapi-jwt')

  let user = null

  if (jwtToken) {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL}/users/me`,
      {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      }
    )

    user = response.data
  }

  return user
}

function useProvideAuth() {
  const [user, setUser] = useState(null)
  const [loading, setLoading] = useState(true)

  const handleUser = async (user) => {
    if (user) {
      setUser(user)
      setLoading(false)
      return user
    } else {
      setUser(null)
      setLoading(false)
      return null
    }
  }

  const signinWithGitHub = () => {
    setLoading(true)
    window.location = `${process.env.NEXT_PUBLIC_API_URL}/connect/github`
  }

  const signout = () => {
    setLoading(true)
    window.location = '/api/auth/logout'
  }

  // useEffect(() => {
  //   const jwtToken = cookieCutter.get('strapi-jwt');
  //   console.log('jwtToken', jwtToken)

  //   if (jwtToken) {
  //     const response = axios.get(`${process.env.NEXT_PUBLIC_API_URL}/users/me`, {
  //       headers: {
  //         'Authentication': `Bearer ${jwtToken}`
  //       }
  //     });

  //     console.log(response);
  //   } else {
  //     handleUser(false)
  //   }
  // }, [])

  return {
    user,
    loading,
    signinWithGitHub,
    signout,
    handleUser,
  }
}
