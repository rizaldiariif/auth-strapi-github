import { checkUserServerSide, useAuth } from '../lib/auth'

import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useEffect } from 'react'

export default function Home({ user }) {
  const auth = useAuth()

  useEffect(() => {
    auth.handleUser(user)
  }, [])

  console.log(auth.user)

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Next.js!</a>
        </h1>

        {auth.loading ? (
          'Loading...'
        ) : !auth.user ? (
          <button onClick={auth.signinWithGitHub}>Log In Github</button>
        ) : (
          <>
            <p className={styles.description}>{auth.user.email}</p>
            <button onClick={auth.signout}>Log Out</button>
          </>
        )}
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

export async function getServerSideProps(context) {
  const user = await checkUserServerSide(context.req, context.res)

  return {
    props: {
      user,
    },
  }
}
