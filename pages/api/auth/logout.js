import Cookies from 'cookies'

export default async (req, res) => {
  // Create a cookies instance
  const cookies = new Cookies(req, res);

  // Set a cookie
  cookies.set('strapi-jwt');

  res.redirect('/');
}
