import axios from 'axios'
import Cookies from 'cookies'

export default async (req, res) => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/auth/github/callback?access_token=${req.query.access_token}`);

  // Create a cookies instance
  const cookies = new Cookies(req, res);

  // Set a cookie
  cookies.set('strapi-jwt', response.data.jwt, {
    httpOnly: true // true by default
  });

  res.redirect('/');
}
